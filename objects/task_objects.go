package objects

type Task struct {
	UserID      int    `json:"user_id" form:"user_id"`
	Title       string `json:"title" form:"title"`
	Description string `json:"description" form:"description"`
	Status      string `json:"status" form:"status"`
}
