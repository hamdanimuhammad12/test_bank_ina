package objects

type Users struct {
	Name     string `json:"name" form:"name" binding:"required"`
	Email    string `json:"email" form:"email"`
	Password string `json:"password" form:"password"`
}

type UserLogin struct {
	UserID string `json:"id"`
	Email  string `json:"email"`
}

type Login struct {
	Email    string `form:"email" json:"email" `
	Password string `form:"password" json:"password" `
}
