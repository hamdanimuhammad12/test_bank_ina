package main

import (
	"fmt"
	"strconv"
	"test_bank_ina/config"
	"test_bank_ina/routes"

	"github.com/spf13/viper"
)

func main() {
	viper.SetConfigName("gifnoc")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetConfigType("yml")
	var conf config.Configurations

	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Error reading config file, %s", err)
	}

	err := viper.Unmarshal(&conf)
	if err != nil {
		fmt.Printf("Unable to decode into struct, %v", err)
	}

	r := routes.SetupRoutes(conf)

	r.Run(":" + strconv.Itoa(conf.Server.Port))
}
