package controllers

import (
	"fmt"
	"net/http"
	"strconv"
	"test_bank_ina/models"
	"test_bank_ina/objects"
	"test_bank_ina/tables"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"golang.org/x/crypto/bcrypt"
)

// interface
type UserController interface {
	RegisterHandler(c *gin.Context)
	ViewUserHandler(c *gin.Context)
	GetUserHandler(c *gin.Context)
	UpdateUserHandler(c *gin.Context)
	DeleteUserHandler(c *gin.Context)
}

type userController struct {
	userMod models.UserModels
}

func NewUserController(userModel models.UserModels) UserController {
	return &userController{
		userMod: userModel,
	}
}

func (ctr *userController) RegisterHandler(c *gin.Context) {
	var reqData objects.Users
	if err := c.ShouldBindJSON(&reqData); err != nil {
		fmt.Println(err)

		var errorMessages []string
		if validationErrors, ok := err.(validator.ValidationErrors); ok {
			for _, e := range validationErrors {
				errorMessage := fmt.Sprintf("Error validating %s, condition: %s", e.Field(), e.ActualTag())
				errorMessages = append(errorMessages, errorMessage)
			}
		} else {
			errorMessages = append(errorMessages, err.Error())
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
		})
		return
	}

	var whreUsers tables.Users
	whreUsers.Email = reqData.Email
	checkEmailResp, _ := ctr.userMod.GetUserWhereRow(whreUsers)

	fmt.Println(checkEmailResp.ID)

	if checkEmailResp.ID == 0 {
		hash, err := Hash(reqData.Password)
		if err != nil {
			fmt.Println(err)
			return
		}

		var userData tables.Users
		userData.Name = reqData.Name
		userData.Email = reqData.Email
		userData.Password = hash

		resUser, err := ctr.userMod.InsertUser(userData)
		if err != nil {
			fmt.Println(err)
			return
		}

		if resUser.ID > 0 {
			c.JSON(http.StatusOK, gin.H{
				"status":  true,
				"message": "Register Success",
			})
			return
		} else {
			c.JSON(http.StatusOK, gin.H{
				"status":  true,
				"message": "Register not Success",
			})
			return
		}

	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"message": "This email has registered before. Please use another account.",
			"data":    nil,
		})
		return
	}

}

func (ctr *userController) ViewUserHandler(c *gin.Context) {
	checkUserResp, err := ctr.userMod.GetUserWhereRows()
	if err != nil {
		fmt.Println(err)
	}
	var userDataV []tables.UserData
	for i := 0; i < len(checkUserResp); i++ {
		var userData tables.UserData
		userData.ID = checkUserResp[i].ID
		userData.Name = checkUserResp[i].Name
		userData.Email = checkUserResp[i].Email

		userDataV = append(userDataV, userData)

	}
	c.JSON(http.StatusOK, gin.H{
		"status":  true,
		"message": "Data Found",
		"data":    userDataV,
	})

	return

}

func (ctr *userController) GetUserHandler(c *gin.Context) {

	userIDV := c.Param("id")
	user_id, err := strconv.Atoi(userIDV)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	var user tables.Users
	user.ID = user_id
	checkUserResp, err := ctr.userMod.GetUserWhereRow(user)
	if err != nil {
		fmt.Println(err)
	}

	if checkUserResp.ID > 0 {
		var userData tables.UserData
		userData.ID = checkUserResp.ID
		userData.Name = checkUserResp.Name
		userData.Email = checkUserResp.Email

		c.JSON(http.StatusOK, gin.H{
			"status":  true,
			"message": "Data Found",
			"data":    userData,
		})
	} else {

		c.JSON(http.StatusNotFound, gin.H{
			"status":  false,
			"message": "Data Not Found",
			"data":    nil,
		})
	}

	return
}

func (ctr *userController) UpdateUserHandler(c *gin.Context) {
	userIDV := c.Param("id")
	user_id, err := strconv.Atoi(userIDV)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	var reqData objects.Users
	if err := c.ShouldBindJSON(&reqData); err != nil {
		fmt.Println(err)
		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error validate %s, condition: %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
		})
		return
	}

	var user tables.Users
	user.ID = user_id
	checkUserResp, err := ctr.userMod.GetUserWhereRow(user)
	if err != nil {
		fmt.Println(err)
	}

	if checkUserResp.ID > 0 {
		var userData tables.Users
		userData.Name = reqData.Name

		// Menggunakan method UpdateStatus dari model untuk memperbarui status
		_, err = ctr.userMod.UpdateUser(user_id, userData)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"error": err,
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status":  true,
			"message": "Status successfully updated",
			"data":    nil,
		})
	} else {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  false,
			"message": "Data Not Found",
			"data":    nil,
		})
	}

	return

}

func (ctr *userController) DeleteUserHandler(c *gin.Context) {
	userIDV := c.Param("id")
	user_id, err := strconv.Atoi(userIDV)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	var user tables.Users
	user.ID = user_id
	checkUserResp, err := ctr.userMod.GetUserWhereRow(user)
	if err != nil {
		fmt.Println(err)
	}

	if checkUserResp.ID > 0 {

		_, err = ctr.userMod.DeleteUser(user_id)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"error": err,
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status":  true,
			"message": "Status successfully deleted",
			"data":    nil,
		})
	} else {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  false,
			"message": "Data Not Found",
			"data":    nil,
		})
	}

	return
}

func Hash(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func VerifyPassword(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
