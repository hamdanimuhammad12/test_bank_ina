package controllers

import (
	"fmt"
	"net/http"
	"strconv"
	"test_bank_ina/models"
	"test_bank_ina/objects"
	"test_bank_ina/tables"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type TaskController interface {
	AddTaskHandler(c *gin.Context)
	ViewTaskHandler(c *gin.Context)
	GetTaskHandler(c *gin.Context)
	UpdateTaskHandler(c *gin.Context)
	DeleteTaskHandler(c *gin.Context)
}

type taskController struct {
	taskMod models.TaskModels
	userMod models.UserModels
}

func NewTaskController(taskModel models.TaskModels, userModel models.UserModels) TaskController {
	return &taskController{
		taskMod: taskModel,
		userMod: userModel,
	}
}

func (ctr *taskController) AddTaskHandler(c *gin.Context) {

	var reqData objects.Task
	err := c.ShouldBindJSON(&reqData)
	if err != nil {
		fmt.Println(err)
		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error validate %s, condition: %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
		})
		return

	}

	var user tables.Users
	user.ID = reqData.UserID
	checkUserResp, err := ctr.userMod.GetUserWhereRow(user)
	if err != nil {
		fmt.Println(err)
	}

	if checkUserResp.ID > 0 {
		var taskData tables.Tasks
		taskData.UserID = reqData.UserID
		taskData.Title = reqData.Title
		taskData.Description = reqData.Description
		taskData.Status = reqData.Status

		resTask, err := ctr.taskMod.InsertTask(taskData)
		if err != nil {
			fmt.Println(err)
			return
		}

		if resTask.ID > 0 {
			c.JSON(http.StatusOK, gin.H{
				"status":  true,
				"message": "Add Task Success",
			})
			return
		} else {
			c.JSON(http.StatusOK, gin.H{
				"status":  true,
				"message": "Add Task not Success",
			})
			return
		}
	} else {

		c.JSON(http.StatusNotFound, gin.H{
			"status":  false,
			"message": "User Not Found",
			"data":    nil,
		})
	}

}

func (ctr *taskController) ViewTaskHandler(c *gin.Context) {
	checkTaskResp, err := ctr.taskMod.GetTaskWhereRows()
	if err != nil {
		fmt.Println(err)
	}
	var taskDataV []tables.TaskData
	for i := 0; i < len(checkTaskResp); i++ {

		var userData tables.Users
		userData.ID = checkTaskResp[i].UserID
		checkuUserResp, err := ctr.userMod.GetUserWhereRow(userData)
		if err != nil {
			fmt.Println(err)
		}

		var taskData tables.TaskData
		taskData.ID = checkTaskResp[i].ID
		taskData.UserID = checkTaskResp[i].UserID
		taskData.Name = checkuUserResp.Name
		taskData.Title = checkTaskResp[i].Title
		taskData.Description = checkTaskResp[i].Description
		taskDataV = append(taskDataV, taskData)

	}
	c.JSON(http.StatusOK, gin.H{
		"status":  true,
		"message": "Data Found",
		"data":    taskDataV,
	})

	return
}

func (ctr *taskController) GetTaskHandler(c *gin.Context) {
	taskIDV := c.Param("id")
	task_id, err := strconv.Atoi(taskIDV)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	var task tables.Tasks
	task.ID = task_id
	checkTaskResp, err := ctr.taskMod.GetTaskWhereRow(task)
	if err != nil {
		fmt.Println(err)
	}

	if checkTaskResp.ID > 0 {

		var userData tables.Users
		userData.ID = checkTaskResp.UserID
		checkuUserResp, err := ctr.userMod.GetUserWhereRow(userData)
		if err != nil {
			fmt.Println(err)
		}

		var taskData tables.TaskData
		taskData.ID = checkTaskResp.ID
		taskData.UserID = checkTaskResp.UserID
		taskData.Name = checkuUserResp.Name
		taskData.Title = checkTaskResp.Title
		taskData.Description = checkTaskResp.Description

		c.JSON(http.StatusOK, gin.H{
			"status":  true,
			"message": "Data Found",
			"data":    taskData,
		})
	} else {

		c.JSON(http.StatusNotFound, gin.H{
			"status":  false,
			"message": "Data Not Found",
			"data":    nil,
		})
	}

	return
}

func (ctr *taskController) UpdateTaskHandler(c *gin.Context) {
	taskIDV := c.Param("id")
	task_id, err := strconv.Atoi(taskIDV)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	var reqData objects.Task
	if err := c.ShouldBindJSON(&reqData); err != nil {
		fmt.Println(err)
		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error validate %s, condition: %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
		})
		return
	}

	var task tables.Tasks
	task.ID = task_id
	checkTaskResp, err := ctr.taskMod.GetTaskWhereRow(task)
	if err != nil {
		fmt.Println(err)
	}

	if checkTaskResp.ID > 0 {
		var taskData tables.Tasks
		taskData.Status = reqData.Status

		// Menggunakan method UpdateStatus dari model untuk memperbarui status
		_, err = ctr.taskMod.UpdateTask(task_id, taskData)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"error": err,
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status":  true,
			"message": "Status successfully updated",
			"data":    nil,
		})
	} else {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  false,
			"message": "Data Not Found",
			"data":    nil,
		})
	}

	return
}

func (ctr *taskController) DeleteTaskHandler(c *gin.Context) {
	taskIDV := c.Param("id")
	task_id, err := strconv.Atoi(taskIDV)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	var task tables.Tasks
	task.ID = task_id
	checkTaskResp, err := ctr.taskMod.GetTaskWhereRow(task)
	if err != nil {
		fmt.Println(err)
	}

	if checkTaskResp.ID > 0 {

		_, err = ctr.taskMod.DeleteTask(task_id)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"error": err,
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status":  true,
			"message": "Status successfully deleted",
			"data":    nil,
		})
	} else {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  false,
			"message": "Data Not Found",
			"data":    nil,
		})
	}

	return
}
