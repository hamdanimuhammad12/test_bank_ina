package middleware

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/mail"
	"test_bank_ina/objects"
	"test_bank_ina/tables"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/oauth2"
	"gorm.io/gorm"
)

var identityKey = "email"

func SetupMiddleware(db *gorm.DB) *jwt.GinJWTMiddleware {

	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:       "jwt",
		Key:         []byte("#testBankINA#"),
		Timeout:     time.Duration(24*365) * time.Hour,
		MaxRefresh:  time.Duration(24*365) * time.Hour,
		IdentityKey: identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {

			if v, ok := data.(*objects.UserLogin); ok {

				tokenResult := jwt.MapClaims{
					identityKey: v.Email,
				}

				return tokenResult
			}

			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &objects.UserLogin{
				Email: claims[identityKey].(string),
			}
		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			if v, ok := data.(*objects.UserLogin); ok {

				var userData tables.Users

				errc := db.Debug().First(&userData, "email = ? ", v.Email).Error
				if errc != nil {
					fmt.Println(errc)
					return false
				}

				if userData.ID > 0 {
					return true
				}
			}

			return false
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {

			var loginVals objects.Login
			if err := c.ShouldBind(&loginVals); err != nil {
				return "", jwt.ErrMissingLoginValues
			}

			var userData tables.Users
			errc := db.Debug().First(&userData, "email = ?", loginVals.Email).Error
			if errc != nil {
				fmt.Println(errc)
			}
			checkPassword := VerifyPassword(loginVals.Password, userData.Password)
			if checkPassword {

				return &objects.UserLogin{
					Email: userData.Email,
				}, nil
			}

			return nil, jwt.ErrFailedAuthentication
		},

		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"status":  false,
				"message": message,
			})
		},
		TokenLookup:   "header: Authorization, query: token, cookie: jwt",
		TokenHeadName: "Bearer",
		TimeFunc:      time.Now,
	})

	if err != nil {
		fmt.Println("Err: ", err)
		return nil
	}

	return authMiddleware
}

func VerifyPassword(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func valid(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}

func OAuthLoginHandler(oauthConfig *oauth2.Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		url := oauthConfig.AuthCodeURL("state", oauth2.AccessTypeOnline)
		c.Redirect(302, url)
	}
}

func OAuthCallbackHandler(oauthConfig *oauth2.Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		code := c.Query("code")

		token, err := oauthConfig.Exchange(context.Background(), code)
		if err != nil {
			c.JSON(500, gin.H{"error": "Failed to exchange code for token"})
			return
		}
		c.JSON(200, gin.H{"token": token})
	}
}
func AuthMiddlewareGoogle(oauthConfig *oauth2.Config) gin.HandlerFunc {
	return func(c *gin.Context) {

		authorizationHeader := c.GetHeader("Authorization")
		if authorizationHeader == "" {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
			c.Abort()
			return
		}

		tokenString := authorizationHeader[len("Bearer "):]

		token := &oauth2.Token{
			AccessToken: tokenString,
			TokenType:   "Bearer",
		}

		client := oauthConfig.Client(context.Background(), token)

		userInfoResponse, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
		if err != nil {
			handleOAuthError(c, "Invalid token")
			return
		}
		defer userInfoResponse.Body.Close()

		body, err := ioutil.ReadAll(userInfoResponse.Body)
		if err != nil {
			handleInternalError(c, "Error reading response body")
			return
		}

		var userInfo map[string]interface{}
		if err := json.Unmarshal(body, &userInfo); err != nil {
			handleInternalError(c, "Error decoding JSON")
			return
		}

		username, usernameOK := userInfo["name"].(string)
		email, emailOK := userInfo["email"].(string)

		if !usernameOK || !emailOK {
			handleInternalError(c, "Username or email not found in response")
			return
		}

		c.Set("username", username)
		c.Set("email", email)

		c.Next()
	}
}

func handleOAuthError(c *gin.Context, errorMessage string) {
	c.JSON(http.StatusUnauthorized, gin.H{"error": errorMessage})
	c.Abort()
}

func handleInternalError(c *gin.Context, errorMessage string) {
	fmt.Println("Error:", errorMessage)
	c.JSON(http.StatusInternalServerError, gin.H{"error": errorMessage})
	c.Abort()
}
