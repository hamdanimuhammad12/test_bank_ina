package tables

import (
	"time"
)

type Tasks struct {
	ID          int    `gorm:"primaryKey;autoIncrement"`
	UserID      int    `gorm:"index"`
	Title       string `gorm:"type:varchar(255)"`
	Description string `gorm:"type:text"`
	Status      string `gorm:"type:varchar(50);default:pending"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

type TaskData struct {
	ID          int    `json:"id"`
	UserID      int    `json:"user_id"`
	Name        string `json:"name"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Status      string `json:"status"`
}
