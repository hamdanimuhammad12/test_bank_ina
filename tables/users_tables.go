package tables

import (
	"time"
)

type Users struct {
	ID        int    `gorm:"primaryKey;autoIncrement"`
	Name      string `gorm:"type:varchar(255)"`
	Email     string `gorm:"type:varchar(255);unique"`
	Password  string `gorm:"type:varchar(255)"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

type UserData struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Email string `gorm:"type:varchar(255);default:null" json:"email"`
}
