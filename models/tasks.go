package models

import (
	"test_bank_ina/tables"

	"gorm.io/gorm"
)

type TaskModels interface {
	InsertTask(task tables.Tasks) (tables.Tasks, error)
	GetTaskWhereRow(user tables.Tasks) (tables.Tasks, error)
	GetTaskWhereRows() ([]tables.TaskData, error)
	UpdateTask(taskID int, task tables.Tasks) (tables.Tasks, error)
	DeleteTask(taskID int) (bool, error)
}

type taskConnection struct {
	db *gorm.DB
}

func NewTaskModels(dbg *gorm.DB) TaskModels {
	return &taskConnection{
		db: dbg,
	}
}

func (con *taskConnection) InsertTask(data tables.Tasks) (tables.Tasks, error) {
	err := con.db.Create(&data).Error
	if err != nil {
		return tables.Tasks{}, err
	}
	return data, nil
}

func (con *taskConnection) GetTaskWhereRow(fields tables.Tasks) (tables.Tasks, error) {
	var data tables.Tasks
	err := con.db.Select("tasks.id, tasks.user_id,tasks.title, tasks.description, tasks.created_at").Where(fields).First(&data).Error
	if err != nil {
		return tables.Tasks{}, err
	}
	return data, nil
}

func (con *taskConnection) GetTaskWhereRows() ([]tables.TaskData, error) {
	var data []tables.TaskData
	err := con.db.Table("tasks").Select("tasks.id, tasks.user_id,tasks.title, tasks.description, tasks.created_at").Find(&data).Error
	if err != nil {
		return data, err
	}
	return data, nil
}

func (con *taskConnection) UpdateTask(taskID int, fields tables.Tasks) (tables.Tasks, error) {
	var data tables.Tasks
	err := con.db.Where("id = ?", taskID).Updates(fields).Error

	return data, err
}

func (con *taskConnection) DeleteTask(taskID int) (bool, error) {
	var data tables.Tasks
	err := con.db.Where("id = ?", taskID).Delete(&data).Error
	if err != nil {
		return false, err
	}

	return true, err
}
