package models

import (
	"fmt"
	"test_bank_ina/tables"

	"gorm.io/gorm"
)

type UserModels interface {
	InsertUser(user tables.Users) (tables.Users, error)
	GetUserWhereRow(user tables.Users) (tables.Users, error)
	GetUserWhereRows() ([]tables.UserData, error)
	UpdateUser(userID int, user tables.Users) (tables.Users, error)
	DeleteUser(userID int) (bool, error)
}

type userConnection struct {
	db *gorm.DB
}

func NewUserModels(dbg *gorm.DB) UserModels {
	return &userConnection{
		db: dbg,
	}
}

func (con *userConnection) InsertUser(data tables.Users) (tables.Users, error) {
	err := con.db.Create(&data).Error
	if err != nil {
		fmt.Println(err)
		return tables.Users{}, err
	}
	return data, nil
}

func (con *userConnection) GetUserWhereRow(fields tables.Users) (tables.Users, error) {
	var data tables.Users
	err := con.db.Select("users.id, users.name, users.email, users.created_at").Where(fields).First(&data).Error
	if err != nil {
		return tables.Users{}, err
	}
	return data, nil
}

func (con *userConnection) GetUserWhereRows() ([]tables.UserData, error) {
	var data []tables.UserData
	err := con.db.Table("users").Select("users.id, users.name, users.email, users.created_at").Find(&data).Error
	if err != nil {
		return data, err
	}
	return data, nil
}

func (con *userConnection) UpdateUser(userID int, fields tables.Users) (tables.Users, error) {
	var data tables.Users
	err := con.db.Where("id = ?", userID).Updates(fields).Error

	return data, err
}

func (con *userConnection) DeleteUser(userID int) (bool, error) {
	var data tables.Users
	err := con.db.Where("id = ?", userID).Delete(&data).Error
	if err != nil {
		return false, err
	}

	return true, err
}
