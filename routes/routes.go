package routes

import (
	"test_bank_ina/config"
	"test_bank_ina/controllers"
	m "test_bank_ina/middleware"
	"test_bank_ina/models"
	"test_bank_ina/tables"

	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
	"gorm.io/gorm"
)

func SetupRoutes(conf config.Configurations) *gin.Engine {

	r := gin.Default()

	var varconf config.Configurations = conf
	var db *gorm.DB = config.ConnectDB(varconf)

	// oauth2 google
	var oauth2Config *oauth2.Config = config.Oauth2(varconf)

	// Run migrations
	db.AutoMigrate(&tables.Users{})
	db.AutoMigrate(&tables.Tasks{})

	// models
	var userModels models.UserModels = models.NewUserModels(db)
	var taskModels models.TaskModels = models.NewTaskModels(db)

	// controller
	var UserController controllers.UserController = controllers.NewUserController(userModels)
	var TaskController controllers.TaskController = controllers.NewTaskController(taskModels, userModels)

	authMiddleware := m.SetupMiddleware(db) // dashboard access

	r.GET("/", func(c *gin.Context) { c.JSON(404, gin.H{"code": 200, "message": "Wellcome to Aplikasi Test Bank INA"}) })
	r.POST("/login", authMiddleware.LoginHandler)

	// Membuat API CRUD untuk Users dan Tasks

	// Users
	r.POST("/register", UserController.RegisterHandler)
	r.GET("/users", UserController.ViewUserHandler)
	r.GET("/users/:id", UserController.GetUserHandler)
	r.PUT("/users/:id", UserController.UpdateUserHandler)
	r.DELETE("/users/:id", UserController.DeleteUserHandler)

	// OAuth mendapatkan token google
	r.GET("/oauth2/login", m.OAuthLoginHandler(oauth2Config))
	r.GET("/oauth2/callback", m.OAuthCallbackHandler(oauth2Config))

	// Menambahkan Autentikasi OAuth 2 untuk Endpoint Task

	// Tasks
	r.POST("/tasks", m.AuthMiddlewareGoogle(oauth2Config), TaskController.AddTaskHandler)
	r.GET("/tasks", m.AuthMiddlewareGoogle(oauth2Config), TaskController.ViewTaskHandler)
	r.GET("/tasks/:id", m.AuthMiddlewareGoogle(oauth2Config), TaskController.GetTaskHandler)
	r.PUT("/tasks/:id", m.AuthMiddlewareGoogle(oauth2Config), TaskController.UpdateTaskHandler)
	r.DELETE("/tasks/:id", m.AuthMiddlewareGoogle(oauth2Config), TaskController.DeleteTaskHandler)

	return r

}
