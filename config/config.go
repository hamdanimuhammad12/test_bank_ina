package config

import (
	"fmt"

	"golang.org/x/oauth2"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Configurations struct {
	Server              ServerConfigurations
	Database            DatabaseConfigurations
	GoogleLoginConfig   oauth2.Config
	FacebookLoginConfig oauth2.Config
	Oauth2              Oauth2Configurations
}

// ServerConfigurations exported
type ServerConfigurations struct {
	Hostname string
	Port     int
	Ssl_Port int
}

// DatabaseConfigurations exported
type DatabaseConfigurations struct {
	DBHost     string
	DBPort     string
	DBName     string
	DBUser     string
	DBPassword string
}

type Oauth2Configurations struct {
	ClientID     string
	ClientSecret string
	RedirectURL  string
}

func ConnectDB(config Configurations) *gorm.DB {
	URL := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		config.Database.DBUser,
		config.Database.DBPassword,
		config.Database.DBHost,
		config.Database.DBPort,
		config.Database.DBName,
	)

	db, err := gorm.Open(mysql.Open(URL), &gorm.Config{})
	if err != nil {
		panic(err.Error())
	}

	return db
}

func Oauth2(config Configurations) *oauth2.Config {
	oauthConfig := &oauth2.Config{
		ClientID:     config.Oauth2.ClientID,
		ClientSecret: config.Oauth2.ClientSecret,
		RedirectURL:  config.Oauth2.RedirectURL,
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"},
		Endpoint: oauth2.Endpoint{
			AuthURL:  "https://accounts.google.com/o/oauth2/auth",
			TokenURL: "https://accounts.google.com/o/oauth2/token",
		},
	}

	return oauthConfig
}
